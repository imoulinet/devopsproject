package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsSonarTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JenkinsSonarTestProjectApplication.class, args);
	}

}
